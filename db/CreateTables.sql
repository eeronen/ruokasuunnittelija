-- Create database
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;

CREATE DATABASE shopping_list CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;

USE shopping_list;

-- Household
CREATE TABLE households (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    share_code varchar(255) NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

-- Foods
CREATE TABLE foods (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    is_cleared boolean NOT NULL DEFAULT false,
    household_id int NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (household_id) REFERENCES households(id)
);

-- Shopping items
CREATE TABLE shopping_items (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    is_done boolean DEFAULT false,
    is_cleared boolean DEFAULT false,
    food_id int NULL,
    household_id int,
    category varchar(255) DEFAULT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (food_id) REFERENCES foods(id),
    FOREIGN KEY (household_id) REFERENCES households(id)
);

-- Users
CREATE TABLE users (
    id int NOT NULL AUTO_INCREMENT,
    api_token varchar(255) NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

-- Accesses
CREATE TABLE accesses (
    id int NOT NULL AUTO_INCREMENT,
    household_id int NOT NULL,
    user_id int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (household_id) REFERENCES households(id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

COMMIT;
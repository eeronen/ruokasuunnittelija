const Path = require("node:path");
const fs = require("node:fs/promises");
const zlib = require("node:zlib");

const args = process.argv.slice(2);

const BUILD_PATH = Path.join(__dirname, "build");
const INDEX_PATH = Path.join(BUILD_PATH, "index.html");

const RESOURCES = ["bundle.css", "bundle.js", "bundle.js.map"];

const date = new Date();
const timeStamp = `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`;
const newResources = RESOURCES.map((fileName) => getNewFileName(fileName));

function getNewFileName(oldName) {
    const fileNameParts = oldName.split(".");
    fileNameParts.splice(1, 0, timeStamp);
    return fileNameParts.join(".");
}

async function renameFiles(from, to) {
    return Promise.all(
        from.map((fileName, index) => {
            const oldPath = Path.join(BUILD_PATH, fileName);
            const newPath = Path.join(BUILD_PATH, to[index]);
            return fs.rename(oldPath, newPath);
        })
    );
}

async function renamePaths(from, to) {
    let indexFileContent = await fs.readFile(INDEX_PATH, "utf-8");
    from.forEach((fileName, index) => {
        indexFileContent = indexFileContent.replace(fileName, to[index]);
    });
    await fs.writeFile(INDEX_PATH, indexFileContent);
}

(async () => {
    if (args[0] === "rollback") {
        console.log("Reseting files..");
        await renameFiles(newResources, RESOURCES);
        console.log("Reseting paths in index..");
        await renamePaths(newResources, RESOURCES);
    } else {
        console.log("Renaming files..");
        await renameFiles(RESOURCES, newResources);
        console.log("Renaming paths in index..");
        await renamePaths(RESOURCES, newResources);
    }
    zlib;
})();

# Ruokasuunnittelija
## Devenv setup
The easiest way to get started is to install Laravel homestead. 
See more info here: https://laravel.com/docs/8.x/homestead#introduction

Then you should run migrations and you should be all set

## Folder structure
The front end is moda using svelte. Backend is using laravel (lumen). The front end is served from laravel, so the builds should go to api/public/

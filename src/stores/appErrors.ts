import { writable } from "svelte/store";

export interface AppError {
    message: string;
    operation: string;
}

function createErrorStore() {
    const { subscribe, update } = writable<AppError[]>([]);
    return {
        subscribe,
        addError: (operation: string, message: string) =>
            update((state) => {
                state.push({ operation, message });
                return state;
            }),
        clearError: (index: number) => {
            update((state) => {
                state.splice(index, 1);
                return state;
            });
        },
    };
}

export const errors = createErrorStore();

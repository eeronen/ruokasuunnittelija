import { derived, writable } from "svelte/store";
import { getCurrentWeeksMondayDate } from "../utils/Date";
import {
    addFood,
    getFoods,
    removeFood,
    renameFood,
} from "../utils/ServerUtils";
import { getNormalizedString } from "../utils/StringUtils";

export const REHEATED = "$REHEATED";
const SYNC_INTERVAL = 15000;

export interface FoodData {
    name: string;
    id?: number;
}

interface FoodNameStoreState {
    foods: FoodData[];
}

const INITIAL_STORE: FoodNameStoreState = {
    foods: [],
};

function createFoodNameStore() {
    const { subscribe, update } = writable(INITIAL_STORE);
    let syncTimeout: null | number = null;
    const sendFoodData = async (foodName: string) => {
        return await addFood({
            name: foodName,
        });
    };

    const updateFoods = async () => {
        clearTimeout(syncTimeout);
        const foods = await getFoods();
        update((state) => {
            state.foods = foods.map((foodData) => ({
                name: foodData.name,
                id: foodData.id,
            }));
            return state;
        });
        syncTimeout = setTimeout(updateFoods, SYNC_INTERVAL);
    };

    const startSyncing = () => {
        updateFoods();
    };

    const stopSyncing = () => {
        clearTimeout(syncTimeout);
        syncTimeout = null;
    };

    return {
        subscribe,
        updateFoods,
        startSyncing,
        stopSyncing,
        addFood: async (foodName: string) => {
            const normalizedName = getNormalizedString(foodName);
            await sendFoodData(normalizedName);
            updateFoods();
        },
        removeFood: async (id: number) => {
            await removeFood(id);
            updateFoods();
        },
        renameFood: async (id: number, newName: string) => {
            const normalizedName = getNormalizedString(newName);
            await renameFood(id, normalizedName);
            updateFoods();
        },
    };
}

export const foodNames = createFoodNameStore();

export const existingFoodNames = derived(foodNames, (foodNamesState) => {
    return foodNamesState.foods.flat().reduce((accum, foodData) => {
        if (foodData === undefined) {
            return accum;
        }
        const food = accum.find((food) => food.name === foodData.name);
        if (food) {
            food.count++;
        } else {
            accum.push({ name: foodData.name, count: 1 });
        }
        return accum;
    }, [] as { name: string; count: number }[]);
});

foodNames.updateFoods();

document.addEventListener("visibilitychange", () => {
    if (document.visibilityState === "visible") {
        foodNames.startSyncing();
    } else {
        foodNames.stopSyncing();
    }
});

foodNames.updateFoods();

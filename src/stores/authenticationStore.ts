import { writable } from "svelte/store";
import { logIn } from "../utils/ServerUtils";

function createAuthenticationStore() {
    const { subscribe, set } = writable(
        window.localStorage.getItem("apiToken") || ""
    );
    return {
        subscribe,
        setApiToken: (token: string) => {
            set(token);
            window.localStorage.setItem("apiToken", token);
        },
        logIn: async (password: string, household: string) => {
            /*
            const apiToken = await logIn(password);
            set(apiToken);
            window.localStorage.setItem("apiToken", apiToken);
            */
        },
    };
}

export const authentication = createAuthenticationStore();

import { derived, writable } from "svelte/store";
import type { IngredientEntry } from "../utils/types";
import {
    addShoppingItem,
    AddShoppingItemPayload,
    deleteShoppingItem,
    getShoppingItems,
    renameShoppingItem,
    sendDoneStatus,
    setCategory,
} from "../utils/ServerUtils";
import { errors } from "./appErrors";
import { CATEGORIES, DEFAULT_CATEGORY } from "../utils/constants";
import { getNormalizedString } from "../utils/StringUtils";

type ShoppingItemState = IngredientEntry[];

const INITIAL_STORE: ShoppingItemState = [];

const SYNC_INTERVAL = 15000;

function createShoppingItemStore() {
    const { subscribe, update, set } = writable(INITIAL_STORE);

    let syncTimeout = null as null | number;

    const syncShoppingItems = async () => {
        clearTimeout(syncTimeout);
        const allItems = await getShoppingItems();
        const newState: ShoppingItemState = allItems.map((item) => ({
            foodName: item.food?.name,
            name: item.name,
            done: !!item.is_done,
            category: item.category,
            foodId: item.food_id,
            id: item.id,
        }));

        set(newState);

        syncTimeout = setTimeout(syncShoppingItems, SYNC_INTERVAL);
    };

    const startSyncing = () => {
        syncShoppingItems();
    };

    const stopSyncing = () => {
        clearTimeout(syncTimeout);
        syncTimeout = null;
    };

    return {
        subscribe,
        syncShoppingItems,
        startSyncing,
        stopSyncing,
        addShoppingItem: async (payload: {
            name: string;
            food?: string;
            foodId?: number;
        }) => {
            clearTimeout(syncTimeout);
            const normalizedName = getNormalizedString(payload.name);
            const newItem: AddShoppingItemPayload = {
                name: normalizedName,
                food: payload.food || null,
                food_id: payload.foodId,
            };

            try {
                await addShoppingItem(newItem);
                // TODO: if the item was not added in 500ms, maybe it should be added to state anyway and shown in grey or something.
            } catch (error) {
                errors.addError("Lisää ostos", error);
            }
            await syncShoppingItems();
        },
        setShoppingItemStatus: async (id: number, isDone: boolean) => {
            clearTimeout(syncTimeout);

            try {
                await sendDoneStatus({
                    id,
                    isDone,
                });
            } catch (error) {
                errors.addError("Merkitse ostos", error);
            }
            await syncShoppingItems();
        },
        removeShoppingItem: async (id: number) => {
            clearTimeout(syncTimeout);
            try {
                await deleteShoppingItem(id);
            } catch (error) {
                errors.addError("Poista ostos", error);
            }
            await syncShoppingItems();
        },
        changeShoppingItemName: async (id: number, newName: string) => {
            clearTimeout(syncTimeout);
            const normalizedName = getNormalizedString(newName);
            try {
                await renameShoppingItem(id, normalizedName);
            } catch (error) {
                errors.addError("Lisää ostos", error);
            }
            await syncShoppingItems();
        },
        setShoppingItemCategory: async (id: number, categoryName: string) => {
            clearTimeout(syncTimeout);
            try {
                await setCategory(id, categoryName);
            } catch (error) {
                errors.addError("Aseta kategoria", error);
            }
            await syncShoppingItems();
        },
    };
}

export const shoppingItems = createShoppingItemStore();

export const categorizedShoppingItems = derived(
    shoppingItems,
    ($shoppingItems) => {
        const categorized = {} as { [categoryName: string]: IngredientEntry[] };
        CATEGORIES.concat(DEFAULT_CATEGORY).forEach(
            (categoryName) => (categorized[categoryName] = [])
        );

        $shoppingItems.forEach((shoppingItem) => {
            if (
                shoppingItem.category &&
                CATEGORIES.includes(shoppingItem.category)
            ) {
                categorized[shoppingItem.category].push(shoppingItem);
            } else {
                categorized[DEFAULT_CATEGORY].push(shoppingItem);
            }
        });

        return categorized;
    }
);

export const shoppingItemsByFoodIdStore = derived(
    shoppingItems,
    ($shoppingItems) => {
        const shoppingItemsByFoodId: { [foodId: number]: IngredientEntry[] } =
            {};
        for (const shoppingItem of $shoppingItems) {
            const foodId = shoppingItem.foodId;
            if (foodId === undefined) {
                continue;
            }

            if (shoppingItemsByFoodId[foodId] === undefined) {
                shoppingItemsByFoodId[foodId] = [shoppingItem];
            } else {
                shoppingItemsByFoodId[foodId].push(shoppingItem);
            }
        }

        return shoppingItemsByFoodId;
    }
);

document.addEventListener("visibilitychange", () => {
    if (document.visibilityState === "visible") {
        shoppingItems.startSyncing();
    } else {
        shoppingItems.stopSyncing();
    }
});

shoppingItems.syncShoppingItems();

import { writable } from "svelte/store";
import { getAccessedHouseholds, logIn } from "../utils/ServerUtils";
import { foodNames } from "./foodNameStore";
import { shoppingItems } from "./shoppingItemStore";

function createHouseholdsStore() {
    const { subscribe, set, update } = writable({
        available: [] as { id: number; name: string; share_code: string }[],
        selected: Number(window.localStorage.getItem("selected_household")) as
            | null
            | number,
        isLoading: true,
    });
    return {
        subscribe,
        addHousehold: async (household: string, password: string) => {
            await logIn(password, household);
        },
        refreshHouseholds: async () => {
            update((state) => {
                state.isLoading = true;
                return state;
            });
            const api_token = window.localStorage.getItem("api_token");
            const accessedHouseholds = await getAccessedHouseholds(api_token);
            if (accessedHouseholds && accessedHouseholds.length > 0) {
                update((state) => {
                    state.available = accessedHouseholds;
                    if (accessedHouseholds.length === 1) {
                        const firstId = accessedHouseholds[0].id;
                        state.selected = firstId;
                        window.localStorage.setItem(
                            "selected_household",
                            firstId.toString()
                        );
                    }
                    return state;
                });
            }
            update((state) => {
                state.isLoading = false;
                return state;
            });
        },
        setSelectedHousehold: (id: number) => {
            update((state) => {
                state.selected = id;
                window.localStorage.setItem(
                    "selected_household",
                    id.toString()
                );
                return state;
            });
            shoppingItems.syncShoppingItems();
            foodNames.updateFoods();
        },
    };
}

export const households = createHouseholdsStore();

import { writable } from "svelte/store";
import type { FoodData } from "./foodNameStore";

export const enum DialogType {
    HouseholdSelectionDialog,
    CategorySelectionDialog,
}

export interface DialogStoreState {
    type: DialogType;
    data: any;
}

function createDialogStore() {
    const { subscribe, update } = writable(
        undefined as DialogStoreState | undefined
    );
    return {
        subscribe,
        openDialog: <DataType>(dialogType: DialogType, data: DataType) =>
            update(() => ({ type: dialogType, data })),
        closeDialogs: () => {
            update(() => undefined);
        },
        updateDialogData: <DataType>(newData: Partial<DataType>) => {
            update((state) => {
                state.data = { ...state.data, ...newData };
                return state;
            });
        },
    };
}

export interface HouseholdSelectionDialogData {}

export interface CategorySelectionDialogData {
    selectedIngredientId: number;
    selectedCategory: string;
    onClose: () => void;
}

export const openedDialog = createDialogStore();

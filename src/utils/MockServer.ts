import type {
    AddFoodPayload,
    GetFoodsResponseItem,
    GetShoppingItemsResponseItem,
} from "./ServerUtils";

class MockServerClass {
    private currentId = 0;
    private foods: GetFoodsResponseItem[];
    private shoppingItems: GetShoppingItemsResponseItem[];
    constructor() {
        this.foods = [];
        this.shoppingItems = [];
    }

    /**
     * fetch
     */
    public fetch(url: string, data: any) {
        if (url === "foods/add") {
            return this.addFood(data);
        } else if (url.includes("foods/get")) {
            const urlParts = url.split("/");
            return this.getFoods(urlParts[2]);
        } else if (url === "shopping_items/add") {
            console.log(url);
        } else if (url === "shopping_items/get") {
            console.log(url);
        } else if (url === "shopping_items/mark_done") {
            console.log(url);
        } else if (url === "shopping_items/remove") {
            console.log(url);
        } else if (url === "shopping_items/rename") {
            console.log(url);
        } else if (url === "household/access") {
            console.log(url);
        } else if (url.includes("household/get_accessed")) {
            console.log(url);
        } else {
            console.log(url);
        }
    }

    private addFood(data: AddFoodPayload[]) {
        this.foods.push(
            ...data.map((food) => {
                this.currentId++;
                const date = new Date();
                return {
                    name: food.name,
                    id: this.currentId,
                    created_at: date.toISOString(),
                    updated_at: date.toISOString(),
                };
            })
        );
    }

    private getFoods(from: string) {
        const fromDate = new Date(from);
        const toDate = new Date(from);
        toDate.setDate(fromDate.getDate() + 6);
        return this.foods;
    }
}

export const MockServer = new MockServerClass();

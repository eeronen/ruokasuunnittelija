import { getFoodSuggestions } from "./ServerUtils";

export async function getPossibleFoodNames() {
    const suggestions = await getFoodSuggestions();
    return suggestions.map((food) => food.name);
}

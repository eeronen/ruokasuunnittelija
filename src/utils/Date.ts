import { DAY_NAMES } from "./constants";

/**
 * Get day name from day number
 */
export function getWeekDayName(dayNumber: number) {
    return DAY_NAMES[dayNumber];
}

/**
 * Get day number from day name
 */
export function getWeekDayNumber(dayName: string) {
    return DAY_NAMES.findIndex((day) => day === dayName);
}

/**
 * Returns a date object from date string
 */
export function getDateJSObject(date: Date) {
    return new Date(date);
}

/**
 * Returns ISO string of current weeks monday date.
 */
export function getCurrentWeeksMondayDate() {
    const today = new Date();
    // In JS sunday is first day of the week
    let dayNumber = today.getDay() - 1;
    if (dayNumber === -1) {
        dayNumber = 6;
    }

    today.setDate(today.getDate() - dayNumber);
    today.setUTCHours(0, 0, 0, 0);
    return today.toISOString();
}

/**
 * Returns the number of whole weeks between two dates
 */
export function getWeeksBetween(from: Date | string, to: Date | string) {
    return Math.floor(getDaysBetween(from, to) / 7);
}

/**
 * Returns the number of days between two dates
 */
export function getDaysBetween(from: Date | string, to: Date | string) {
    let date1 = typeof from === "string" ? new Date(from) : from;
    let date2 = typeof to === "string" ? new Date(to) : to;

    return Math.round(
        (date2.getTime() - date1.getTime()) / (1000 * 60 * 60 * 24)
    );
}

/**
 * Returns a string representation of weeks between
 * given date and today.
 */
export function getRelativeWeekString(date: Date | string) {
    const thisMonday = getCurrentWeeksMondayDate();
    const weeksBetween = getWeeksBetween(thisMonday, date);

    if (weeksBetween === 0) {
        return "Tälle viikolle";
    } else if (weeksBetween === 1) {
        return "Ensi viikolle";
    } else if (weeksBetween > 1) {
        return `${weeksBetween} viikon päähän`;
    } else if (weeksBetween === -1) {
        return "Viime viikolle";
    } else {
        return `${weeksBetween * -1} viikoa sitten`;
    }
}

import { getApiToken, getSelectedHouseholdId, setApiToken } from "./AuthUtils";

export interface AddFoodPayload {
    name: string;
}

export interface GetFoodsResponseItem {
    id: number;
    name: string;
    created_at: string;
    updated_at: string;
}

export interface AddShoppingItemPayload {
    name: string;
    // FIXME: tämä pitäisi olla food ID
    food?: null | string;
    food_id?: number;
}

export interface GetShoppingItemsResponseItem {
    created_at: string;
    food_id: null | number;
    id: number;
    is_done: number;
    name: string;
    updated_at: string;
    food: GetFoodsResponseItem;
    category: string | null;
}

export interface LogInPayload {
    api_token: string;
    password: string;
    household: string;
}

export interface SendDoneStatusesPayload {
    id: number;
    isDone: boolean;
}

export interface CreateHouseholdPayload {
    name: string;
    password: string;
    email: string;
}

export async function createHousehold(data: CreateHouseholdPayload) {
    return postData("household/add.php", data);
}

export async function addFood(foodData: AddFoodPayload) {
    return postData("foods/add.php", foodData);
}

export async function renameFood(id: number, newName: string) {
    return postData("foods/rename.php", { id, newName });
}

export async function getFoods() {
    return getData<GetFoodsResponseItem[]>(`foods/get.php`, { addToken: true });
}

export async function getFoodSuggestions() {
    return getData<GetFoodsResponseItem[]>("foods/suggestions.php", {
        addToken: true,
    });
}

export async function removeFood(id: number) {
    return deleteData("foods/remove.php", {
        id,
    });
}

export async function addShoppingItem(item: AddShoppingItemPayload) {
    return postData("shopping_items/add.php", item);
}

export async function getShoppingItems() {
    return getData<GetShoppingItemsResponseItem[]>("shopping_items/get.php", {
        addToken: true,
    });
}

export async function sendDoneStatus(item: SendDoneStatusesPayload) {
    return postData("shopping_items/mark_done.php", item);
}

export async function clearDoneShoppingItems() {
    return postData("shopping_items/clear_done_items.php", {});
}

export async function deleteShoppingItem(id: number) {
    return deleteData("shopping_items/remove.php", { id });
}

export async function renameShoppingItem(id: number, newName: string) {
    return postData("shopping_items/rename.php", { id, newName });
}

export async function logIn(password: string, household: string) {
    const token = await postData("household/access.php", {
        password,
        household,
        api_token: window.localStorage.getItem("api_token"),
    });
    setApiToken(token);
}

export async function logInWithShareCode(code: string) {
    const token = await postData("household/access.php", {
        share_code: code,
    });
    setApiToken(token);
}

export async function setCategory(id: number, category: string | null) {
    return postData("shopping_items/set_category.php", { id, category });
}

export async function getAccessedHouseholds(apiToken: string) {
    try {
        const response = await getData(
            `household/get_accessed.php?api_token=${apiToken}`
        );
        return response;
    } catch (_) {
        return [];
    }
}

function getApiRequestUrl(route: string, addToken = false) {
    const baseRoute = `${window.origin}/api/${route}`;
    if (addToken) {
        return `${baseRoute}?household_id=${getSelectedHouseholdId()}&api_token=${getApiToken()}`;
    } else {
        return baseRoute;
    }
}

interface RequestOptions {
    addToken?: boolean;
}

interface PayloadRequestOptions {
    omitToken?: boolean;
}

async function postData(
    url: string,
    data: any,
    options?: PayloadRequestOptions
) {
    const response = await fetch(getApiRequestUrl(url), {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: getPayload(data, options),
    });
    const responseText = await response.text();
    if (response.status !== 200) {
        throw new Error(`${response.status} ${responseText}`);
    }
    return responseText;
}

async function getData<ResponseType = any>(
    url: string,
    options?: RequestOptions
) {
    const response = await fetch(getApiRequestUrl(url, options?.addToken));
    if (response.status !== 200) {
        throw new Error(`${response.status} ${response.statusText}`);
    }
    return response.json() as Promise<ResponseType>;
}

async function deleteData(
    url: string,
    data: any,
    options?: PayloadRequestOptions
) {
    const response = await fetch(getApiRequestUrl(url), {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        body: getPayload(data, options),
    });
    if (response.status !== 200) {
        throw new Error(`${response.status} ${response.statusText}`);
    }
    return response.text();
}

function getPayload(data: any, options?: PayloadRequestOptions): string {
    if (options && options.omitToken) {
        return JSON.stringify(data);
    } else {
        const payload = { ...data };
        payload.api_token = getApiToken();
        payload.household_id = getSelectedHouseholdId();
        return JSON.stringify(payload);
    }
}

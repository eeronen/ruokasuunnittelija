export interface IngredientEntry {
    id: number | null;
    name: string;
    done: boolean;
    foodName?: string;
    foodId?: number;
    category?: string;
    isSyncing?: boolean;
}

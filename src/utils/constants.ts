export const DAY_NAMES = [
    "Maanantai",
    "Tiistai",
    "Keskiviikko",
    "Torstai",
    "Perjantai",
    "Lauantai",
    "Sunnuntai",
];

export const CATEGORIES = [
    "Hedelmät ja vihannekset",
    "Lihat ja kalat",
    "Maitotuotteet",
    "Kuiva-aineet",
    "Leivät ja leivonnaiset",
    "Säilykkeet",
    "Mausteet",
    "Pakasteet ja einekset",
    "Juomat",
    "Herkut",
    "Kodinhoito",
];

export const DEFAULT_CATEGORY = "Muut";

export const TRUE_STRING = "true";
export const FALSE_STRING = "false";

export const getBooleanAsString = (value: boolean) =>
    value ? TRUE_STRING : FALSE_STRING;

export const getStringAsBoolean = (value: string) =>
    value === TRUE_STRING ? true : false;

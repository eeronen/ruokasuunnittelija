const HOUSEHOLD_KEY = "selected_household";
const API_TOKEN_KEY = "api_token";

export function getSelectedHouseholdId() {
    return window.localStorage.getItem(HOUSEHOLD_KEY);
}

export function getApiToken() {
    return window.localStorage.getItem(API_TOKEN_KEY);
}

export function setApiToken(token: string) {
    return window.localStorage.setItem(API_TOKEN_KEY, token);
}

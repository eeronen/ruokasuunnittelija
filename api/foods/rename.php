<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_payload($payload, ["household_id", "id", "newName"]);

$category_response = execute_query(
    "UPDATE foods SET name = ? WHERE id = ? AND household_id = ?",
    "sii",
    [$payload["newName"], $payload["id"], $payload["household_id"]]
);

?>
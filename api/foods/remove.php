<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/access.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_query_params($payload, ["household_id", "id"]);

check_access($payload["household_id"]);

execute_query(
    "DELETE shopping_items
    FROM shopping_items
    WHERE foodId = ?",
    "i",
    [$payload["id"]]
);

execute_query(
    "DELETE FROM foods
    WHERE household_id = ? AND id = ? AND is_cleared = false",
    "ii",
    [$payload["household_id"], $payload["id"]]
);

?>
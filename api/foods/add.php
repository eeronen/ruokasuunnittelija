<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/access.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_payload($payload, ["household_id", "name"]);

check_access($payload["household_id"]);

execute_query(
    "INSERT INTO foods (name, household_id) VALUES (?, ?)",
    "si",
    [$payload["name"], $payload["household_id"]]
);

echo $mysqli->insert_id;

?>
<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/access.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_payload($payload, ["household_id", "name"]);

check_access($payload["household_id"]);

execute_query(
    "UPDATE foods
    SET is_cleared = 1
    WHERE household_id = ? AND name = ? AND is_cleared = 0",
    "is",
    [$payload["household_id"], $payload["name"]]
);

?>
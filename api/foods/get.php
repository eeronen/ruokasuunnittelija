<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/access.php";
include_once "../utils/query.php";

verify_query_params(["household_id"]);

check_access($_GET["household_id"]);

$food_results = execute_query(
    "SELECT id, name FROM foods WHERE household_id = ? AND is_cleared = false",
    "i",
    [$_GET["household_id"]]
);

$food_names = $food_results->fetch_all(MYSQLI_ASSOC);

echo json_encode($food_names);
?>
<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/crypto.php";
include_once "../utils/access.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_payload($payload, ["name", "password", "email"]);

$name = $payload["name"];

$results = execute_query(
    "SELECT name FROM households WHERE name=(?)",
    "s",
    [$name]
);
$row = $results->fetch_assoc();

if ($row) {
    http_response_code(400);
    die("Household with the same name already exists");
}

$password = get_hashed($payload["password"]);
$email = $payload["email"];
$share_code = bin2hex(random_bytes(16));

$add_household = execute_query(
    "INSERT INTO households (name, password, email, share_code) VALUES (?, ?, ?, ?)",
    "ssss",
    [$name, $password, $email, $share_code]
);
$household_id = $mysqli->insert_id;

$user_id = add_access($household_id);

echo get_api_token($user_id);
?>
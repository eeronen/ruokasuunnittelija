<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/crypto.php";
include_once "../utils/query.php";
include_once "../utils/access.php";

$payload = get_payload();

$household_results;

if (isset($payload["share_code"])) {
    $results = execute_query(
        "SELECT id FROM households WHERE share_code = ?",
        "s",
        [$payload["share_code"]]
    );
} else {
    verify_payload($payload, ["household", "password"]);
    
    $results = execute_query(
        "SELECT id, password FROM households WHERE name=?",
        "s",
        [$payload["household"]]
    );
}

$household = $results->fetch_assoc();

if (!$household) {
    http_response_code(404);
    die("No household found with given name");
}

if (!isset($payload["share_code"])) {
    $given_password = get_hashed($payload["password"]);
    
    if ($given_password !== $household["password"]) {
        http_response_code(401);
        die("Incorrect household password");
    }
}

$user_id = add_access($household["id"]);

echo get_api_token($user_id);

?>
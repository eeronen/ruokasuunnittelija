<?php
include_once "../utils/connection.php";
include_once "../utils/user.php";
include_once "../utils/query.php";

$user_id = get_user_id(false);

$access_results = execute_query(
    "SELECT households.id, households.name, households.share_code
    FROM accesses
    INNER JOIN households 
    ON accesses.household_id = households.id 
    WHERE user_id = ?",
    "i",
    [$user_id]
);

echo json_encode($access_results->fetch_all(MYSQLI_ASSOC));
?>
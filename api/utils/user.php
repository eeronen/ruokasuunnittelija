<?php
include_once "query.php";
include_once "payload.php";

function get_user_id(bool $create) : string | null {
    global $mysqli;

    $given_token = get_request_item("api_token");

    $user_results = execute_query(
        "SELECT id FROM users WHERE api_token=?",
        "s",
        [$given_token]
    );
    $user_row = $user_results->fetch_assoc();
    
    if (!$user_row && $create) {
        $api_token = bin2hex(random_bytes(20));
        $add_user = execute_query(
            "INSERT INTO users (api_token) VALUES (?)",
            "s",
            [$api_token]
        );
        $user_id = $mysqli->insert_id;
        return $user_id;
    } elseif (!$user_row && !$create) {
        return NULL;
    } else {
        return $user_row["id"];
    }
}

function get_api_token($user_id): string | null {
    global $mysqli;

    $user_results = execute_query(
        "SELECT api_token FROM users WHERE id=?",
        "i",
        [$user_id]
    );
    $user_row = $user_results->fetch_assoc();
    return $user_row["api_token"];
}

?>
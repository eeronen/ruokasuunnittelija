<?php
include_once "user.php";
include_once "query.php";

function add_access(string $household_id) {
    $user_id = get_user_id(true);

    $existing_access = execute_query(
        "SELECT id FROM accesses WHERE household_id = ? AND user_id = ?",
        "ii",
        [$household_id, $user_id]
    );

    if ($existing_access->num_rows > 0) {
        http_response_code(400);
        die("Given user already has access to the given household");
    }

    $add_access = execute_query(
        "INSERT INTO accesses (household_id, user_id) VALUES (?, ?)",
        "ii",
        [$household_id, $user_id]
    );

    return $user_id;
}

function check_access(string $household_id) {
    $user_id = get_user_id(false);

    $access_results = execute_query(
        "SELECT * FROM accesses WHERE user_id = ? AND household_id = ?",
        "ii",
        [$user_id, $household_id]
    );

    if ($access_results->num_rows === 0) {
        http_response_code(401);
        die("Current user doesn't have an access for given household");
    }
}
?>
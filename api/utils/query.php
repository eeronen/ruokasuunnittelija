<?php
include_once "connection.php";

function execute_query(string $query,string $param_types, ?array $params) {
    global $mysqli;

    $stmt = $mysqli->prepare($query);
    $stmt->bind_param($param_types, ...$params);
    $stmt->execute();
    return $stmt->get_result();
}
?>
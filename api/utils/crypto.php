<?php
function get_hashed(string $password) {
    return hash("sha256", $password);
}
?>
<?php
function get_payload() {
    return json_decode(file_get_contents("php://input"), true);
}

function get_request_item(string $name) {
    $payload = get_payload();

    if (isset($_GET[$name])) {
        return $_GET[$name];
    } else if (isset($payload[$name])) {
        return $payload[$name];
    } else {
        return NULL;
    }
}

function verify_payload(array $payload, array $fields) {
    foreach ($fields as $field) {
        if (!isset($payload[$field])) {
            http_response_code(400);
            die($field . " name was not set in the payload");
        }
    }
}

function verify_query_params(array $fields) {
    foreach ($fields as $field) {
        if (!isset($_GET[$field])) {
            http_response_code(400);
            die($field . " name was not set in the payload");
        }
    }
}
?>
<?php include_once "environment.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$mysqli = new mysqli($servername, $username, $password, $database);
$mysqli->set_charset('utf8');

if (!$mysqli) {
    die("Connection failed: " . mysqli_connect_error());
} 
?>

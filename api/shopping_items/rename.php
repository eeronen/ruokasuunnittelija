<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_payload($payload, ["household_id", "id", "newName"]);

$category_response = execute_query(
    "UPDATE shopping_items SET name = ? WHERE id = ? AND household_id = ? AND is_cleared = 0",
    "sii",
    [$payload["newName"], $payload["id"], $payload["household_id"]]
);

?>
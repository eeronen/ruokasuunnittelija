<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_payload($payload, ["household_id", "name"]);

$category = NULL;

$category_response = execute_query(
    "SELECT category FROM shopping_items WHERE name = ? AND category IS NOT NULL",
    "s",
    [$payload["name"]]
);

if ($category_response->num_rows > 0) {
    $category = $category_response->fetch_assoc()["category"];
}

$food_id = isset($payload["food_id"]) ? $payload["food_id"] : NULL;

// TODO: hopefully this can be removed at some point
if ($food_id == NULL && isset($payload["food"])) {
    $food_id_response = execute_query(
        "SELECT id from foods WHERE name = ? AND is_cleared = 0",
        "s",
        [$payload["food"]]
    );
    if ($food_id_response->num_rows > 0) {
        $food_id = $food_id_response->fetch_assoc()["id"];
    }
}

execute_query(
    "INSERT INTO shopping_items (name, food_id, household_id, category)
    VALUES (?, ?, ?, ?)",
    "siis",
    [$payload["name"], $food_id, $payload["household_id"], $category]
);

echo $category;

?>
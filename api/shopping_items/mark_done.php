<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_payload($payload, ["household_id", "id", "isDone"]);

$category_response = execute_query(
    "UPDATE shopping_items SET is_done = ? WHERE id = ? AND household_id = ?",
    "iis",
    [$payload["isDone"], $payload["id"], $payload["household_id"]]
);

?>
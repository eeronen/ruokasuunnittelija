<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/query.php";
include_once "../utils/access.php";
include_once "../utils/connection.php";

verify_query_params(["household_id"]);

check_access($_GET["household_id"]);

$results = execute_query(
    "SELECT id, name, is_done, food_id, category
    FROM shopping_items
    WHERE household_id = ? AND is_cleared = false",
    "i",
    [$_GET["household_id"]]
);

echo json_encode($results->fetch_all(MYSQLI_ASSOC));

?>
<?php
include_once "../utils/connection.php";
include_once "../utils/payload.php";
include_once "../utils/query.php";

$payload = get_payload();

verify_payload($payload, ["household_id"]);

execute_query(
    "UPDATE shopping_items SET is_cleared = 1 WHERE household_id = ? AND is_done = 1",
    "s",
    [$payload["household_id"]]
);

$food_results = execute_query(
    "SELECT id FROM foods WHERE household_id = ? AND is_cleared = false",
    "i",
    [$payload["household_id"]]
);

while ($food = mysqli_fetch_assoc($food_results)) {
    $food_id = $food["id"];
    $uncleared_items_results = execute_query(
        "SELECT id FROM shopping_items WHERE food_id = ? AND is_cleared = 0",
        "i",
        [$food_id]
    );
    if ($uncleared_items_results->num_rows == 0) {
        execute_query(
            "UPDATE foods SET is_cleared = 1 WHERE id = ?",
            "i",
            [$food_id]
        );
    }
}
?>
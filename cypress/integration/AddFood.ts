describe("Add foods", () => {
    before(() => {
        cy.on("uncaught:exception", () => false);
        cy.visit("http://homestead.ruokasuunnittelija/");
        cy.get("input").type("pwd");
        cy.get("button").click();
        cy.contains("Ruokalista");
    });

    it("Should open food adding dialog", () => {
        cy.get(".food-cell").contains("+").click();
        cy.contains("Maanantai lounas");
    });

    it("Should have food name input", () => {
        cy.get("input").first().should("be.empty");
        cy.get("input").first().type("Testiruoka");
    });

    it("Should be closeable", () => {
        cy.contains("Ok").click();
        cy.contains("Maanatai lounas").should("not.exist");
    });

    it("Should have added new food", () => {
        cy.get(".food-cell").contains("Testiruoka");
    });
});
